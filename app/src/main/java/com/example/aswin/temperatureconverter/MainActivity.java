package com.example.aswin.temperatureconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtCelsius;
    TextView tvFahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCelsius = findViewById(R.id.txtCelsius);
        tvFahrenheit = findViewById(R.id.tvFahrenheit);

        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey("result")) {
                String result = savedInstanceState.getString("result");
                tvFahrenheit.setText(result);
            }
        }
    }

    public void convertTemperature(View view) {
        float celsius = Float.parseFloat(txtCelsius.getText().toString());
        float fahrenheit = celsius * 9 / 5 + 32;

        tvFahrenheit.setText(fahrenheit + " F");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String result = tvFahrenheit.getText().toString();
        outState.putString("result", result);

        super.onSaveInstanceState(outState);
    }
}
